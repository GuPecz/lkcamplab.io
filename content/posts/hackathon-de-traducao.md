---
title: "Hackathon de Tradução"
date: 2022-09-08T00:13:31-03:00
draft: false
categories: ["Tutorial"]
tags: ["Hackathon", "Outreachy"]
authors: ["Pedro Sader Azevedo", "Isabella Breder"]
---

Para o primeiro hackathon presencial do LKCAMP, escolhemos uma das contribuições mais valorizadas no mundo *open source*: a tradução.

A tradução amplia o acesso à tecnologia, e serve como "porta de entrada" para a colaboração em software. Isto, pois o fluxo de contribuição com tradução é exatamente o mesmo que o de contribuição com código!

Neste guia de tradução de projetos de código aberto vamos abordar:

<!-- vim-markdown-toc GFM -->

* [Plataformas de Hospedagem de Código](#plataformas-de-hospedagem-de-código)
  * [Passo 0: Instalar programas](#passo-0-instalar-programas)
  * [Passo 1: Configurar seu usuário git local](#passo-1-configurar-seu-usuário-git-local)
  * [Passo 2: Escolher um projeto](#passo-2-escolher-um-projeto)
  * [Passo 3: Criar uma conta](#passo-3-criar-uma-conta)
  * [Passo 4: Configurar acesso à conta](#passo-4-configurar-acesso-à-conta)
  * [Passo 5: Bifurcar o projeto](#passo-5-bifurcar-o-projeto)
  * [Passo 6: Obter uma cópia local](#passo-6-obter-uma-cópia-local)
  * [Passo 7: Colocar a mão na massa](#passo-7-colocar-a-mão-na-massa)
    * [Passo i: Configurar o GTranslator](#passo-i-configurar-o-gtranslator)
    * [Passo ii: Traduzir o programa](#passo-ii-traduzir-o-programa)
    * [Passo iii: Atualizar LINGUAS](#passo-iii-atualizar-linguas)
  * [Passo 8: Pedir uma revisão](#passo-8-pedir-uma-revisão)
  * [Passo 9: Submeter sua contribuição](#passo-9-submeter-sua-contribuição)
  * [Passo 10: Rodar o programa traduzido (desafio)](#passo-10-rodar-o-programa-traduzido-desafio)
  * [Passo 11: Adicionar-se aos créditos (desafio)](#passo-11-adicionar-se-aos-créditos-desafio)
* [Plataformas de Tradução Online](#plataformas-de-tradução-online)

<!-- vim-markdown-toc -->

# Plataformas de Hospedagem de Código

Muitos projetos *open source* armazenam seu código fonte em plataformas de hospedagem de código, como [GitHub](https://github.com/), [GitLab](https://gitlab.com/), [BitBucket](https://bitbucket.org/), [Codeberg](https://codeberg.org/), [Gitea](https://gitea.io/en-us/), etc. Além de guardar o código, essas plataformas facilitam o gerenciamento de contribuições, o reportamento de falhas, a comunicação com usuários, a publicação de pacotes, etc. 

O desenvolvimento de software aberto ocorre majoritariamente nessas plataformas e, por isso, focaremos nelas neste hackathon.

## Passo 0: Instalar programas

Para o Hackathon de Tradução, são necessários dois programas: git para versionamento, e GTranslator para edição dos arquivos de tradução. Para instalá-los, use os comandos abaixo, dependendo da sua distribuição GNU/Linux.

Se sua distribuição usa `dnf` como gerenciador de pacotes (Fedora, RHEL, CentOS, Rocky Linux, etc)

{{< highlight bash >}}
sudo dnf install git-all gtranslator
{{< / highlight >}}

Se sua distribuição usa `apt` como gerenciador de pacotes (Debian, Ubuntu, Linux Mint, Pop!_OS, Zorin OS, elementary OS, Kali Linux, etc):

{{< highlight bash >}}
sudo apt install git-all gtranslator
{{< / highlight >}}

Se sua distribuição usa `pacman` como gerenciador de pacotes (Arch Linux, Arco Linux, Manjaro, Garuda Linux, EndeavorOS, etc)

{{< highlight bash >}}
sudo pacman -S git gtranslator
{{< / highlight >}}

Caso encontre dificuldades durante a instalação, chame um monitor!

## Passo 1: Configurar seu usuário git local

Para conferir se o git está instalado, basta utilizar o comando:

{{< highlight bash >}}
git --version
{{< / highlight >}}

E, para checar seus dados locais, utilize:

{{< highlight bash >}}
git config user.email
{{< / highlight >}}
{{< highlight bash >}}
git config user.name
{{< / highlight >}}

No caso de ainda não estar configurado, basta rodar os seguintes comandos:

{{< highlight bash >}}
git config --global user.name "Nome Sobrenome"
{{< / highlight >}}
{{< highlight bash >}}
git config --global user.email emaildaconta@exemplo.com
{{< / highlight >}}

{{< alerts info
"Utilize as aspas duplas somente na hora de configurar o nome; para o e-mail não as utilize. É importante que seu e-mail do git local seja o mesmo e-mail utilizado para criar a conta no GitHub e GitLab"
>}}

Após realizar esse processo, você deve ter uma saída parecida com a minha nas configurações do git, com seu nome e e-mail informados:

![](/imgs/posts/hackathon-de-traducao/gitconfig.png)

## Passo 2: Escolher um projeto

Elaboramos uma planilha com diversos projetos que precisam de tradução para o portguês brasileiro. Você pode acessar essa planilha clicando [aqui](https://ethercalc.net/gshwkx2wg554). Para o evento de hoje, recomendamos projetos que tenham "Arquivo .po" como esquema de tradução e "Ausente" como status de tradução.

Você pode ler uma descrição de cada projeto clicando na aba "Comments" da planilha, ou simplesmente pairando o mouse sobre o nome do projeto.

![](/imgs/posts/hackathon-de-traducao/planilha.png)

Quando escolher para qual projeto você vai contribuir, escreva seu nome (e o nome dos demais integrantes da sua equipe, caso esteja fazendo a tradução em grupo). Em seguida, clique no link associado ao projeto e siga para o próximo passo.

## Passo 3: Criar uma conta

Para contribuir para um projeto *open source*, é necessário criar uma conta na plataforma que hospeda o seu código.

É possível que você tenha observado que essa planilha tem mais de um link diferente para o GitLab. Isso acontece porque o GitLab tem seu *server side* aberto, então qualquer um pode hospedar seu próprio GitLab. Por exemplo, a empresa que desenvolve o GitLab tem um servidor oficial (https://gitlab.com) mas o projeto GNOME usa o próprio servidor (gitlab.gnome.org) assim como a Unicamp (https://gitlab.unicamp.br).

Esses servidores tipicamente não se comunicam entre si, então você precisa criar uma conta em cada uma delas. Para não confundir suas credenciais posteriormente, recomendamos o uso de um gerenciador de senhas.

## Passo 4: Configurar acesso à conta

Por motivos de segurança, não é possível fazer login no GitHub pelo terminal usando apenas nome de usuário e senha. É necessário criar um *Personal Access Token* (PAT).

Para isso, vá à pagina inicial do [GitHub](https://github.com/) e clique no botão do seu perfil, no canto superior direito da tela. Selecione "*Settings*".

![](/imgs/posts/hackathon-de-traducao/pat01.png)

Selecione a opção "Developer Settings", último item da barra lateral.

![](/imgs/posts/hackathon-de-traducao/pat02.png)

Clique na opção "*Personal access tokens*".

![](/imgs/posts/hackathon-de-traducao/pat03.png)

Selecione "*Generate new token*".

![](/imgs/posts/hackathon-de-traducao/pat04.png)

Depois de confirmar sua senha, configure o escopo de acesso do PAT criado. Para o hackathon, é necessário que `repo` esteja habilitado.

{{< alerts error
"**Aviso:** Ao gerar um PAT, cole o conteúdo dele em algum lugar que possa acessar depois (preferencialmente um gerenciador de senhas), pois ele só é visível no momento em que é criado. Se o perder, será necessário gerar um novo token."
>}}

## Passo 5: Bifurcar o projeto

Como exemplo ilustrativo para o restante do tutorial vou fazer o processo de tradução do aplicativo [Organizer](https://gitlab.gnome.org/aviwad/organizer), cujo repositório está hospedado no servidor de GitLab do GNOME.

{{< alerts info
"É denominado \"repositório\" o conjunto de arquivos versionados conjuntamente pelo git. Esse é um termo que vamos usar muito daqui pra frente, então chame um monitor caso não tenha ficado claro!"
>}}

Enfim, agora que você configurou a sua conta no GitHub ou no GitLab, use-a para criar uma cópia do repositório original no seu próprio perfil. Para isso, siga o link associado ao projeto de sua escolha e clique em Fork.


![](/imgs/posts/hackathon-de-traducao/fork.png)

## Passo 6: Obter uma cópia local

Para editar o código, você precisa de uma cópia local (ou seja, no seu próprio computador) do repositório. Para isso, use o comando:

{{< highlight bash >}}
git clone <link-do-repositório>
{{< / highlight >}}

Como eu estou fazendo o exemplo do Organizer, vou usar o comando:

{{< highlight bash >}}
git clone https://gitlab.gnome.org/aviwad/organizer
{{< / highlight >}}

Esse comando vai criar uma pasta com o nome do projeto (no meu caso, `organizer`) contendo todos os arquivos do mesmo. Entre na pasta do repositório usando o seguinte comando:

{{< highlight bash >}}
cd <nome-do-projeto>
{{< / highlight >}}

Antes de fazer qualquer mudança, é uma boa prática criar e usar uma nova *branch* no git. Para isso, use o comando:

{{< highlight bash >}}
git checkout -b translate-ptbr
{{< / highlight >}}

Com isso, temos graficamente:

![](/imgs/posts/hackathon-de-traducao/clone.png)

## Passo 7: Colocar a mão na massa

Finalmente, tudo pronto para começar a traduzir!

### Passo i: Configurar o GTranslator

Abra o GTranslator e siga o setup inicial. Preencha informações como nome e endereço de email com cuidado, pois isso será usado para te dar os créditos pela tradução.

### Passo ii: Traduzir o programa

Na raiz do repositório há uma pasta chamada `po`, onde você deve encontrar um arquivo com extensão `.pot` (o "`t`" é de "*template*"). Crie uma cópia desse arquivo com o nome `pt_BR.po` e abra-o usando o GTranslator.

Se não houver um arquivo com extensão `.pot`, você pode criar o arquivo `pt_BR.po` a partir de outro arquivo `.po`. Por exemplo:`es.po` será o arquivo de tradução de para o espanhol).

{{< alerts warning
"Se um arquivo `pt.po` já existir, abra-o com o GTranslator e verifique se há diferenças suficientes no português usado (Portugal, Moçambique, Cabo Verde, etc) para o português brasileiro. Se sim, pode criar o arquivo `pt_BR.po` mesmo assim."
>}}

O GTranslator irá mostrar uma lista de strings a serem traduzidas, cuja edição pode ser feita no campo logo abaixo dessa lista. Além disso, o GTranslator ajuda a manter a formatação original, detectando e avisando ocorrências como espaços duplos ou pontuações incorretas.

![](/imgs/posts/hackathon-de-traducao/gtranslator.png)

Escreva as equivalências em português para cada string e fique a vontade para tirar qualquer dúvida que surgir durante esse processo.

### Passo iii: Atualizar LINGUAS

O arquivo `LINGUAS` lista todos os idiomas para os quais o programa foi traduzido. Então, para finalizar o processo de tradução em si, adicione "pt_BR" ao arquivo `LINGUAS`.

## Passo 8: Pedir uma revisão

Quando terminar de traduzir todas as strings, busque um colega que esteja na mesma situação e revisem as traduções um do outro. É normal deixar passar alguns errinhos mesmo depois de reler nosso próprio trabalho, por isso a revisão de terceiros é tão crucial para a qualidade da tradução.

Vocês podem simplesmente trocar de lugar ou usar o comando `git clone` com o link do repositório da tradução que você vai revisar.

## Passo 9: Submeter sua contribuição

Utilize os seguintes comandos, na raiz do repositório, para adicionar sua tradução ao versionamento local:

{{< highlight bash >}}
git add po/
git commit -m "add translation to Brazilian Portuguese"
git push origin translate-ptbr
{{< / highlight >}}

Isso vai atualizar a cópia que você tem no seu perfil, a partir da qual você poderá submeter sua contribuição para o projeto original. Para isso, você deve abrir um *merge request* (MR), no caso do GitLab, ou *pull request* (PR), no caso do GitHub.

![](/imgs/posts/hackathon-de-traducao/mr.png)

Ambos oferecem uma interface intuitiva para fazer isso, mas mesmo assim quando chegar aqui, chame um monitor!

## Passo 10: Rodar o programa traduzido (desafio)

Caso queira ver sua tradução "em ação", é necessário compilar e rodar o programa. Projetos *open source* frequentemente documentam esse processo (pacotes para instalar, comandos para rodar, etc). Essa documentação é tipicamente encontrada nos arquivos `README.md` ou `CONTRIBUTING.md`, na raiz do projeto.

Essa etapa exige mais conhecimentos técnicos, por isso a consideramos um desafio! De qualquer maneira, recomendamos que você tente fazê-la caso tenha tempo, pois irá te preparar para os nossos próximos Hackathons.

Se tiver alguma dúvida nessa etapa, não hesite em perguntar!

## Passo 11: Adicionar-se aos créditos (desafio)

Muitos aplicativos têm uma tela de créditos, onde agradecem os contribuidores do projeto. No passo anterior você pôde verificar se seu nome está contemplado nos créditos do projeto (talvez seja necessário deixar o seu sistema em português para validar isso) então, caso não esteja, seu desafio final é mudar isso!

Isso provavelmente exigirá conhecimentos de GTK, framework de desenvolvimento de aplicativos para linux, que será o tema do nosso próximo hackathon.

# Plataformas de Tradução Online

Alguns projetos utilizam plataformas de tradução online, como [Weblate](https://weblate.org) e [Crowdin](https://crowdin.com/), para gerenciar esse tipo de contribuição. Essas plataformas são mais intuitivas que as ferramentas de versionamento de software que ensinamos acima, assim diminuindo as barreiras de entrada para tradutores.

Como o principal objetivo do Hackathon de Tradução era ensinar o fluxo de contribuição para projetos *open source*, focamos nas ferramentas de versionamento de software. No entanto, planejamos extender essa seção para explicar como utilizar as plataformas de tradução online.

Obrigado por participar do Hackathon Tradução! Até a próxima :wink:
