---
title: "Guia de Instalação do Ubuntu"
date: 2022-05-04
draft: false
tags: ["Install Fest 2022", "Ubuntu"]
categories: ["Tutorial"]
authors: ["Pedro Sader Azevedo"]
---

Neste guia, ensinaremos o processo de instalação do Ubuntu Desktop 22.04 LTS. O Ubuntu é uma distribuição linux estável e intuitiva, amplamente considerada como excelente porta de entrada ao mundo dos sistemas operacionais livres.

Dividimos a instalação em passos, que agrupamos de acordo com o contexto em que eles são realizados ("No Windows", "Na firmware", "No ambiente live", e "No Ubuntu").

Vamos nessa!

---

# No Windows

O Windows é o sistema operacional que assumimos que você já tem instalado no seu laptop. Nele, vamos fazer algumas preparações para a instalação do Ubuntu.

### Passo 0: Fazer backup

Antes de começar o processo de instalação, é altamente recomendado fazer backup dos seus arquivos.

{{< alerts error
  "O risco de perda de dados durante o processo de instalação do Ubuntu é baixo, mas não é nulo!"
>}}

Backups podem ser feitos de forma simples, por exemplo colocando seus arquivos mais importantes em um pen drive ou em um serviço confiável de armazenamento em nuvem.


### Passo 1: Liberar espaço no disco

Antes de instalar o Ubuntu, é necessário criar um espaço vazio no seu disco (uma partição sem dados e sem formatação).

{{< alerts warning
"Se o seu laptop é dual drive (tem um SSD e um HDD embutidos), você precisa instalar o Ubuntu no SSD. Se não tiver espaço suficiente no SSD para a instalação completa, você pode instalar os arquivos do sistema no SSD e os arquivos do usuário no HDD. Se esse for o seu caso, busque ajuda!"
>}}

Para isso, clique no menu iniciar usando o botão direito do mouse e selecione "Gerenciamento de Disco".

Clique com o botão direito do mouse no disco `C:` (ou outro disco que você tenha disponível, como `D:`, `E:`, etc) e selecione "Diminuir volume":

![](/imgs/posts/guia-de-instalacao-do-ubuntu/particao03.png)

Digite o espaço que você quer alocar para o Ubuntu. O requerimento mínimo de espaço é 25 GB (25000 MB), mas recomendamos ao menos 50 GB (50000 MB) para poder instalar mais programas. Pra vocês terem ideia, eu ocupo 150GB (150000 MB) do meu disco usando linux *full-time*.

![](/imgs/posts/guia-de-instalacao-do-ubuntu/particao04.png)

Feito isso, confirme a operação clicando em "Diminuir":

![](/imgs/posts/guia-de-instalacao-do-ubuntu/particao05.png)

Você verá que o espaço vazio foi criado:

![](/imgs/posts/guia-de-instalacao-do-ubuntu/particao06.png)

### Passo 2: Desabilitar hibernação e encriptação

A funcionalidade de hibernação do Windows impede o desligamento completo do computador, que será necessário para acessar o Ubuntu quando ele estiver instalado. Por isso, vamos desabilitar essa funcionalidade.

Para isso, abra uma janela do Prompt de Comando como administrador clicando no seu ícone com o botão direito do mouse e selecionando "Executar como administrador".

![](/imgs/posts/guia-de-instalacao-do-ubuntu/tutorial00.png)

Em seguida, digite o comando:

{{< highlight powershell >}}
powercfg -h off
{{< / highlight >}}

É recomendado também desabilitar o BitLocker, ferramenta de encriptação de disco, caso você tenha acesso a essa funcionalidade (exclusiva do Windows Pro e Enterprise). Para fazer isso, digite no mesmo console de antes:

{{< highlight powershell >}}
manage-bde -off C:
{{< / highlight >}}

Isso vai desencriptar o disco `C:`. Caso precise desencriptar outro disco, Substitua o `C:` no comando acima pelo nome do disco (`D:`, `E:`, etc).

### Passo 3: Acessar as configurações de firmware

Nesse momento, **insira o pen drive de instalação** na porta USB do seu laptop (vou explicar o porquê mais adiante).

Agora, vamos acessar as configurações de firmware pelo Windows. Para isso, clique no botão de desligar enquanto segura a tecla Shift do teclado. Ainda segurando a tecla Shift, clique em "Reiniciar".

Seu laptop vai ser inicializado em uma tela azul com várias opções. Selecione "Resolução de Problemas", depois "Opções avançadas", depois "Definições de Firmware UEFI", depois "Reiniciar". O laptop será reiniciado novamente, dessa vez dando acesso às configurações de firmware.

Outra maneira de acessar as configurações de firmware é pressionando uma tecla durante o processo de inicialização do laptop. No entanto, essa tecla não é sempre a mesma (pode ser Esc, F2, F10, F11, etc) e é difícil acertar o *timing* de apertar a tecla durante o processo de inicialização (às vezes precisa segurar, às vezes precisa apertar várias vezes, etc). Por isso, recomendamos usar o método ensinado acima.

---

# Na firmware

A firmware (chamada "UEFI" nos computadores mais novos, e "BiOS" nos mais antigos) é o primeira coisa que liga no seu laptop, sendo ela responsável por gerenciar os processos de inicialização dos sistemas operacionais instalados. A aparência do menu de configurações de firmware varia conforme a fabricante do laptop, mas costuma ser meio tosca. Mesmo assim, será suficiente para nossos propósitos!

### Passo 4: Priorizar inicialização por pen drive

Para podermos usar o pen drive de instalação do Ubuntu, precisamos configurar a firmware para priorizá-lo durante a inicialização. Às vezes essa opção só aparece quando você já tem um pen drive conectado, por isso inserimos o pen drive no laptop no passo anterior!

{{< alerts info
"A tela de configuração de firmware é tipicamente em inglês. Se isso te trouxer dificuldades, não hesite em buscar ajuda."
>}}

Precisamos acessar o menu de configuração de ordem de inicialização ("*Boot order*"). Quando encontrar esse menu, configure o pen drive ("*External flash drive*") como primeira opção.

Em seguida, reinicie seu laptop.

---

# No ambiente live

O ambiente live é o sistema inicializado pelo próprio pen drive de instalação. Vamos usá-lo para testar e para instalar o Ubuntu.

{{< alerts warning
"Nenhuma alteração feita no ambiente live é permanente, ou persiste no sistema instalado. Isso inclui programas instalados, configuração alteradas, arquivos baixados, etc."
>}}

### Passo 5: Testar seu hardware

Assim que seu computador for reiniciado, você será saudado por esse glorioso menuzinho:

![](/imgs/posts/guia-de-instalacao-do-ubuntu/live01.png)

Aperte Enter para ser levado ao ambiente live! Na tela de boas vindas,selecione "Português do Brasil" como idioma e clique em "Experimentar o Ubuntu":

![](/imgs/posts/guia-de-instalacao-do-ubuntu/live02.png)


Agora é um bom momento para verificar a compatibilidade do seu hardware com o kernel linux. Aí vai uma checklist do que testar:

- Som
- Teclado
- Touchpad
- Tela touch
- WiFi
- Bluetooth
- Webcam
- Microfone
- Placa de vídeo

Para testar o som, verifique se [essa música](https://www.youtube.com/watch?v=dQw4w9WgXcQ) toca nitidamente.

No caso do teclado, verifique se todas as teclas funcionam (especialmente as teclas de mídia: :play_button: :pause_button: :stop_button:), as luzes indicadoras de Caps Lock e Num Lock (se tiver), e a iluminação traseira (se tiver). Para testar a webcam e o microfone, use o aplicativo chamado "Cheese".

Conectar-se a eduroam dá trabalho (que será jogado fora quando sairmos do ambiente live!), então recomendamos o uso do compartilhamento de rede do celular pra testar a WiFi. Por fim, teste o bluetooth usando um mouse e/ou um fone de ouvido sem fio.

{{< alerts warning
"Em computadores que possuem placas de vídeo da Nvidia ([as quais têm histórico de encrenca com o kernel linux](https://www.youtube.com/watch?v=iYWzMvlj2RQ)), aproveite o ambiente live para se certificar de que está tudo correndo bem."
>}}

### Passo 6: Instalar o Ubuntu

Se tudo correu bem no passo anterior, vamos à instalação!

Clique no ícone do instalador do Ubuntu:

![](/imgs/posts/guia-de-instalacao-do-ubuntu/live03.png)

Selecione o idioma e o layout do teclado:

![](/imgs/posts/guia-de-instalacao-do-ubuntu/live04.png)

![](/imgs/posts/guia-de-instalacao-do-ubuntu/live05.png)

Na tela seguinte você pode escolher os componentes que serão as instalados. Se você souber que seu computador possui componentes que dependem de drivers proprietários (placas de vídeo da Nvidia, chips de WiFi Maverick, etc), recomendamos assinalar a terceira opção. Se você não souber, assinale a terceira opção por precaução.

![](/imgs/posts/guia-de-instalacao-do-ubuntu/live06.png)

Se você quiser manter o Windows junto ao Ubuntu no seu computador, selecione a opção "**Instalar o Ubuntu ao lado do Windows 10**". Se quiser remover o Windows e usar apenas o Ubuntu, selecione a opção "**Apagar o disco e instalar o Ubuntu**".

{{< alerts warning
"Se o seu computador for dual drive (com um SSD e um HDD embutidos), selecione \"**Opção Avançada**\" e busque ajuda."
>}}

Por fim, escolha o fuso horário no mapa e crie seu usuário. Clique em "Continuar" e aguarde a instalação.

Quando o processo de instalação terminar, clique em "Reiniciar agora" e remova o pen drive de instalação.

Se você optou por manter o Windows, ao reiniciar o computador você deverá ver um menuzinho preto e branco com as opções de inicialização (Windows, Ubuntu, e firmware). Se você optou por remover o Windows, ao reiniciar o computador você deverá ser levado diretamente ao Ubuntu.

{{< alerts error
"Caso o menuzinho não apareça ou o Ubuntu não seja inicializado diretamente, chame ajuda."
>}}

---

# No Ubuntu

O sistema operacional livre, leve, e seguro instalado no seu computador!

### Passo 7: Fazer um ponto de recuperação do sistema

Antes de mais nada, vamos fazer um ponto de recuperação do sistema (ou *snapshot*). Isso permite retornar ao estado de "instalação limpa" sem precisar reinstalar o sistema, caso algo dê errado no futuro.

Para isso vamos usar o aplicativo Timeshift, que você pode instalar pela loja de aplicativos ou pelo terminal.

{{< alerts info
"Nos primeiros minutos de uso, o Ubuntu vai estar baixando o catálogo de programas disponíveis, então talvez você precise esperar um pouco para baixar o Timeshift (tanto pela loja, quanto pelo terminal)."
>}}

No terminal, o comando para instalar o Timeshift é:

{{< highlight shell >}}
sudo apt install timeshift
{{< / highlight >}}

Abra o Timeshift pelo menu de aplicativos e faça o setup inicial, mantendo todas as opções padrão do aplicativo:

![](/imgs/posts/guia-de-instalacao-do-ubuntu/timeshift01.png)
![](/imgs/posts/guia-de-instalacao-do-ubuntu/timeshift02.png)

Clique no botão "Criar" no canto superior direito da janela para fazer um novo *snapshot*. Ao final do processo, anote uma descrição para o *snapshot*.

![](/imgs/posts/guia-de-instalacao-do-ubuntu/timeshift04.png)

Recomendamos fazer um *snapshot* antes de instalar pacotes ou atualizações grandes! E não se preocupe quanto ao armazenamento ocupado por eles: quanto mais *snapshots* você faz, menos espaço eles ocupam individualmente.


### Passo 8: Instalar drivers adicionais

Para componentes do seu laptop que não têm drivers integrados ao kernel linux, vamos instalar drivers adicionais. Isso é feito de forma simples: basta abrir o aplicativo "Drivers Adicionais" e os drivers faltantes vão ser detectados e baixados automaticamente.

### Passo 9: Instalar componentes restritos

Alguns componentes essenciais a experiência do usuário são restringidos por direitos autorais e patentes, por isso não são incluídos no Ubuntu por padrão. Para instalar esses componentes, abra um terminal e digite o seguinte comando:

{{< highlight shell >}}
sudo apt install ubuntu-restricted-extras
{{< / highlight >}}

Esse passo é inteiramente opcional, pois involve o uso de software que não é plenamente livre.

### Passo 10: Aprender a usar o sistema

Nós, do LKCAMP, vamos fazer um curso de linux para todos os participantes do Install Fest 2022 e demais interessados. Focaremos inicialmente no uso pessoal, depois no uso para desenvolvimento de software.

A primeira aula será sexta-feira que vem, nessa mesma sala, e abordará:

- Uso do interface do Ubuntu
- Gerenciamento de pacotes
- Programas essenciais
- Customização

Nos vemos lá!

---

Isso conclui o tutorial! Se restou alguma dúvida, recomendamos entrar no grupo de Telegram que criamos para isso:

| [Grupo Ajuda Linux](https://t.me/linuxunicamp)|
|-----------|
| ![](/imgs/posts/guia-de-instalacao-do-ubuntu/telegram.jpg)|
